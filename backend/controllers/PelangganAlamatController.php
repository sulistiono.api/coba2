<?php

namespace backend\controllers;

use Yii;
use backend\models\PelangganAlamat;
use backend\models\PelangganAlamatSearch;
use backend\models\InfLokasi;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


/**
 * PelangganAlamatController implements the CRUD actions for PelangganAlamat model.
 */
class PelangganAlamatController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PelangganAlamat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PelangganAlamatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PelangganAlamat model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PelangganAlamat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PelangganAlamat();
        $d_provinsi=ArrayHelper::map(InfLokasi::find()
        ->where(" RIGHT(lokasi_kode,10) = '00.00.0000' ")
        ->orderBy(['lokasi_nama'=>SORT_ASC])
        ->all(),'lokasi_propinsi','lokasi_nama');

        $d_kota=ArrayHelper::map(InfLokasi::find()
        ->where(" LEFT(lokasi_kode,2)='35' and  RIGHT(lokasi_kode,7) = '00.0000'and lokasi_kode != '35.00.00.0000' ")
        ->orderBy(['lokasi_nama'=>SORT_ASC])
        ->all(),'lokasi_kode','lokasi_nama');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kode]);
        }

        return $this->render('create', [
            'model' => $model,
            'd_provinsi'=>$d_provinsi,
            'd_kota'=>$d_kota,
        ]);
    }
    

    public function actionKota($id)
    {
        $idlokasi=$id.".00.00.0000";
        $count=InfLokasi::find()
        ->where(" LEFT(lokasi_kode,2)='$id' and  RIGHT(lokasi_kode,7) = '00.0000'and lokasi_kode != '$idlokasi' ")
        ->orderBy(['lokasi_nama'=>SORT_ASC])
        ->count();

        $lokasis=InfLokasi::find()
        ->where(" LEFT(lokasi_kode,2)='$id' and  RIGHT(lokasi_kode,7) = '00.0000'and lokasi_kode != '$idlokasi' ")
        ->orderBy(['lokasi_nama'=>SORT_ASC])
        ->all();

       if($count>0){
         foreach($lokasis as $lokasi){
        echo "<option value='".$lokasi->lokasi_kabupatenkota."'>".$lokasi->lokasi_nama."</option>";
         };


       }else{
        echo "<option>-</option>";
       }


    }



    public function actionKecamatan($prov,$kota)
    {

        //and lokasi_kode != '$idlokasi'   and lokasi_kode != '$idlokasi' 
        $idlokasi=$prov.".".$kota.".00.0000";
        $id=$prov.".".$kota;
        $count=InfLokasi::find()
        ->where(" LEFT(lokasi_kode,5)='$id' and  RIGHT(lokasi_kode,4) = '0000'")
        ->orderBy(['lokasi_nama'=>SORT_ASC])
        ->count();

        $lokasis=InfLokasi::find()
        ->where(" LEFT(lokasi_kode,5)='$id' and  RIGHT(lokasi_kode,4) = '0000'")
        ->orderBy(['lokasi_nama'=>SORT_ASC])
        ->all();

       if($count>0){
         foreach($lokasis as $lokasi){
        echo "<option value='".$lokasi->lokasi_kecamatan."'>".$lokasi->lokasi_nama."</option>";
         };


       }else{
        echo "<option>-</option>";
       }


    }
    /**
     * Updates an existing PelangganAlamat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kode]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PelangganAlamat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PelangganAlamat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PelangganAlamat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PelangganAlamat::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
