<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PesananDetail */

$this->title = 'Create Pesanan Detail';
$this->params['breadcrumbs'][] = ['label' => 'Pesanan Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pesanan-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
