<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PesananDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pesanan Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pesanan-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pesanan Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kd_pesan',
            'nama',
            'harga',
            'jumlah',
            //'total',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
