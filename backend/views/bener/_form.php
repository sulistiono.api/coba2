<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Bener */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bener-form">

    <?php $form = ActiveForm::begin(['options'=>['entype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'gambar')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model,'f_gambar')->fileInput(); ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
