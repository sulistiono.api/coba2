<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Bener */

$this->title = 'Create Bener';
$this->params['breadcrumbs'][] = ['label' => 'Beners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bener-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
