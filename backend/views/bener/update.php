<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Bener */

$this->title = 'Update Bener: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Beners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bener-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
