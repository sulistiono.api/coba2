<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Pesanan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pesanan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_pel')->textInput() ?>

    <?= $form->field($model, 'total_potongan')->textInput() ?>

    <?= $form->field($model, 'kode_unik')->textInput() ?>

    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_berat')->textInput() ?>

    <?= $form->field($model, 'total_bayar')->textInput() ?>

    <?= $form->field($model, 'total_ongkir')->textInput() ?>

    <?= $form->field($model, 'kd_tranfer')->textInput() ?>

    <?= $form->field($model, 'detail_po')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_resi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jumlah_tranfer')->textInput() ?>

    <?= $form->field($model, 'no_rek_pel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_rek_sonya')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl_transfer')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
