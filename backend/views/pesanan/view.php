<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Pesanan */

$this->title = $model->kd_pesan;
$this->params['breadcrumbs'][] = ['label' => 'Pesanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pesanan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->kd_pesan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->kd_pesan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_pesan',
            'kd_pel',
            'total_potongan',
            'kode_unik',
            'alamat',
            'total_berat',
            'total_bayar',
            'total_ongkir',
            'kd_tranfer',
            'detail_po',
            'no_resi',
            'jumlah_tranfer',
            'no_rek_pel',
            'no_rek_sonya',
            'tgl_transfer',
            'status',
        ],
    ]) ?>

</div>
