<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Pesanan */

$this->title = 'Update Pesanan: ' . $model->kd_pesan;
$this->params['breadcrumbs'][] = ['label' => 'Pesanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_pesan, 'url' => ['view', 'id' => $model->kd_pesan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pesanan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
