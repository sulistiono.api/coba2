<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PesananSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pesanan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kd_pesan') ?>

    <?= $form->field($model, 'kd_pel') ?>

    <?= $form->field($model, 'total_potongan') ?>

    <?= $form->field($model, 'kode_unik') ?>

    <?= $form->field($model, 'alamat') ?>

    <?php // echo $form->field($model, 'total_berat') ?>

    <?php // echo $form->field($model, 'total_bayar') ?>

    <?php // echo $form->field($model, 'total_ongkir') ?>

    <?php // echo $form->field($model, 'kd_tranfer') ?>

    <?php // echo $form->field($model, 'detail_po') ?>

    <?php // echo $form->field($model, 'no_resi') ?>

    <?php // echo $form->field($model, 'jumlah_tranfer') ?>

    <?php // echo $form->field($model, 'no_rek_pel') ?>

    <?php // echo $form->field($model, 'no_rek_sonya') ?>

    <?php // echo $form->field($model, 'tgl_transfer') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
