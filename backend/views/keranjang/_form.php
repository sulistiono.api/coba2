<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Keranjang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keranjang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_pel')->textInput() ?>

    <?= $form->field($model, 'kd_barang')->textInput() ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
