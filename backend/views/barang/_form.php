<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Barang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="barang-form">

    <?php $form = ActiveForm::begin(['options'=>['entype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'jenis')->widget(Select2::classname(), [
    'data' => $d_jenis,
    'language' => 'id',
    'options' => ['placeholder' => 'Pilih jenis barang ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    ]); ?>
    

    <?= $form->field($model, 'toko')->dropDownList($d_toko,['prompt'=>'Pilih Toko']) ?>

    <?= $form->field($model, 'harga')->textInput() ?>

    <?= $form->field($model, 'harga_promo')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'berat')->textInput() ?>

    <?= $form->field($model, 'stok')->textInput() ?>

    <?= $form->field($model, 'gambar')->textInput(['maxlength' => true]) ?>

    

    <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
