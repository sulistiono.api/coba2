<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PelangganAlamatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pelanggan Alamats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggan-alamat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pelanggan Alamat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kode',
            'kode_kirim',
            'nama',
            'jk',
            'alamat',
            //'kodepos',
            //'provinsi',
            //'kota_kabupaten',
            //'kecamatan',
            //'telp',
            //'kode_pel',
            //'dropsipe',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
