<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PelangganAlamat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pelanggan-alamat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_kirim')->textInput() ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kodepos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provinsi')->dropDownList($d_provinsi,[
        'prompt'=>'Pilih Provinsi',
        'onchange'=>'
        $.post("index.php?r=pelanggan-alamat/kota&id="+$(this).val(),function(data){
           $("select#pelangganalamat-kota_kabupaten").html(data);
        });
        ',
        ]) ?>
    
    <?= $form->field($model, 'kota_kabupaten')->dropDownList($d_kota,[
        'prompt'=>'Pilih Kota Kabupaten',
        'onchange'=>'
        $.post("index.php?r=pelanggan-alamat/kecamatan&prov="+$("#pelangganalamat-provinsi").val()+"&kota="+$(this).val(),function(data){
           $("select#pelangganalamat-kecamatan").html(data);
        });
        ',
        ]) ?>
    
    <?= $form->field($model, 'kecamatan')->dropDownList($d_kota,['prompt'=>'Pilih Kecamatan',]) ?>
    <?php $form->field($model, 'kecamatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kode_pel')->textInput() ?>

    <?= $form->field($model, 'dropsipe')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
