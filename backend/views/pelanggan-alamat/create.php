<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PelangganAlamat */

$this->title = 'Create Pelanggan Alamat';
$this->params['breadcrumbs'][] = ['label' => 'Pelanggan Alamats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggan-alamat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'd_provinsi'=>$d_provinsi,
        'd_kota'=>$d_kota,
    ]) ?>

</div>
