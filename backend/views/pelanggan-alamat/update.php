<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PelangganAlamat */

$this->title = 'Update Pelanggan Alamat: ' . $model->kode;
$this->params['breadcrumbs'][] = ['label' => 'Pelanggan Alamats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode, 'url' => ['view', 'id' => $model->kode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pelanggan-alamat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
