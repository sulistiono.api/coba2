<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Favorit */

$this->title = 'Create Favorit';
$this->params['breadcrumbs'][] = ['label' => 'Favorits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="favorit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
