<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Favorit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="favorit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_pel')->textInput() ?>

    <?= $form->field($model, 'kd_barang')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
