<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_berita".
 *
 * @property int $kd
 * @property string $judul
 * @property string $isi
 * @property string $tanggal
 */
class Berita extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_berita';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul', 'isi', 'tanggal'], 'required'],
            [['tanggal'], 'safe'],
            [['judul'], 'string', 'max' => 100],
            [['isi'], 'string', 'max' => 5500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd' => 'Kd',
            'judul' => 'Judul',
            'isi' => 'Isi',
            'tanggal' => 'Tanggal',
        ];
    }
}
