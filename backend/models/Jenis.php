<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_jenis".
 *
 * @property int $kd_jenis
 * @property string $nama
 * @property string $tgl_acua
 * @property int $lama_po
 * @property int $lama_produksi
 *
 * @property TbBarang[] $tbBarangs
 */
class Jenis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_jenis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'tgl_acua', 'lama_po', 'lama_produksi'], 'required'],
            [['tgl_acua'], 'safe'],
            [['lama_po', 'lama_produksi'], 'integer'],
            [['nama'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_jenis' => 'Kd Jenis',
            'nama' => 'Nama',
            'tgl_acua' => 'Tgl Acuan',
            'lama_po' => 'Lama Po',
            'lama_produksi' => 'Lama Produksi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbBarangs()
    {
        return $this->hasMany(TbBarang::className(), ['jenis' => 'kd_jenis']);
    }
}
