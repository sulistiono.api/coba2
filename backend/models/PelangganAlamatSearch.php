<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PelangganAlamat;

/**
 * PelangganAlamatSearch represents the model behind the search form of `backend\models\PelangganAlamat`.
 */
class PelangganAlamatSearch extends PelangganAlamat
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode', 'kode_kirim', 'kode_pel'], 'integer'],
            [['nama', 'jk', 'alamat', 'kodepos', 'provinsi', 'kota_kabupaten', 'kecamatan', 'telp', 'dropsipe', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PelangganAlamat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kode' => $this->kode,
            'kode_kirim' => $this->kode_kirim,
            'kode_pel' => $this->kode_pel,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jk', $this->jk])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'kodepos', $this->kodepos])
            ->andFilterWhere(['like', 'provinsi', $this->provinsi])
            ->andFilterWhere(['like', 'kota_kabupaten', $this->kota_kabupaten])
            ->andFilterWhere(['like', 'kecamatan', $this->kecamatan])
            ->andFilterWhere(['like', 'telp', $this->telp])
            ->andFilterWhere(['like', 'dropsipe', $this->dropsipe])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
