<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_pesanan".
 *
 * @property int $kd_pesan
 * @property int $kd_pel
 * @property int $total_potongan
 * @property int $kode_unik
 * @property string $alamat
 * @property double $total_berat
 * @property int $total_bayar
 * @property int $total_ongkir
 * @property int $kd_tranfer
 * @property string $detail_po
 * @property string $no_resi
 * @property int $jumlah_tranfer
 * @property string $no_rek_pel
 * @property string $no_rek_sonya
 * @property string $tgl_transfer
 * @property string $status
 *
 * @property TbPesananDetail[] $tbPesananDetails
 * @property TbTestimoni[] $tbTestimonis
 */
class Pesanan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_pesanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_pel', 'total_potongan', 'kode_unik', 'alamat', 'total_berat', 'total_bayar', 'total_ongkir', 'kd_tranfer', 'detail_po', 'no_resi', 'jumlah_tranfer', 'no_rek_pel', 'no_rek_sonya', 'tgl_transfer', 'status'], 'required'],
            [['kd_pel', 'total_potongan', 'kode_unik', 'total_bayar', 'total_ongkir', 'kd_tranfer', 'jumlah_tranfer'], 'integer'],
            [['total_berat'], 'number'],
            [['tgl_transfer'], 'safe'],
            [['alamat', 'detail_po'], 'string', 'max' => 250],
            [['no_resi', 'status'], 'string', 'max' => 100],
            [['no_rek_pel', 'no_rek_sonya'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_pesan' => 'Kd Pesan',
            'kd_pel' => 'Kd Pel',
            'total_potongan' => 'Total Potongan',
            'kode_unik' => 'Kode Unik',
            'alamat' => 'Alamat',
            'total_berat' => 'Total Berat',
            'total_bayar' => 'Total Bayar',
            'total_ongkir' => 'Total Ongkir',
            'kd_tranfer' => 'Kd Tranfer',
            'detail_po' => 'Detail Po',
            'no_resi' => 'No Resi',
            'jumlah_tranfer' => 'Jumlah Tranfer',
            'no_rek_pel' => 'No Rek Pel',
            'no_rek_sonya' => 'No Rek Sonya',
            'tgl_transfer' => 'Tgl Transfer',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbPesananDetails()
    {
        return $this->hasMany(PesananDetail::className(), ['kd_pesan' => 'kd_pesan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbTestimonis()
    {
        return $this->hasMany(Testimoni::className(), ['kd_pesan' => 'kd_pesan']);
    }
}
