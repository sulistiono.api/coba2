<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Barang;

/**
 * BarangSearch represents the model behind the search form of `backend\models\Barang`.
 */
class BarangSearch extends Barang
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_barang', 'harga', 'harga_promo', 'stok', 'created_at', 'updated_at'], 'integer'],
            [['nama','jenis', 'toko', 'status', 'gambar', 'deskripsi'], 'safe'],
            [['berat'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Barang::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['jenis0','toko0']);

        // grid filtering conditions
        $query->andFilterWhere([
            'kd_barang' => $this->kd_barang,
            'harga' => $this->harga,
            'harga_promo' => $this->harga_promo,
            'berat' => $this->berat,
            'stok' => $this->stok,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'tb_barang.nama', $this->nama])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'gambar', $this->gambar])
            ->andFilterWhere(['like', 'tb_jenis.nama', $this->jenis])
            ->andFilterWhere(['like', 'tb_toko.nama_toko', $this->toko])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi]);

        return $dataProvider;
    }
}
