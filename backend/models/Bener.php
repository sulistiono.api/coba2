<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_bener".
 *
 * @property int $id
 * @property string $gambar
 */
class Bener extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_bener';
    }

    /**
     * {@inheritdoc}
     */
    public $f_gambar;
            
    public function rules()
    {
        return [
            [['gambar'], 'required'],
            [['f_gambar'],'file'],
            [['gambar'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gambar' => 'Gambar',
        ];
    }
}
