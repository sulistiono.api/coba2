<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_pesanan_detail".
 *
 * @property int $id
 * @property int $kd_pesan
 * @property string $nama
 * @property int $harga
 * @property int $jumlah
 * @property int $total
 *
 * @property TbPesanan $kdPesan
 */
class PesananDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_pesanan_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_pesan', 'nama', 'harga', 'jumlah', 'total'], 'required'],
            [['kd_pesan', 'harga', 'jumlah', 'total'], 'integer'],
            [['nama'], 'string', 'max' => 100],
            [['kd_pesan'], 'exist', 'skipOnError' => true, 'targetClass' => Pesanan::className(), 'targetAttribute' => ['kd_pesan' => 'kd_pesan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_pesan' => 'Kd Pesan',
            'nama' => 'Nama',
            'harga' => 'Harga',
            'jumlah' => 'Jumlah',
            'total' => 'Total',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdPesan()
    {
        return $this->hasOne(Pesanan::className(), ['kd_pesan' => 'kd_pesan']);
    }
}
