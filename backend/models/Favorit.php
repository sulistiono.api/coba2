<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_favorit".
 *
 * @property int $id
 * @property int $kd_pel
 * @property int $kd_barang
 *
 * @property TbBarang $kdBarang
 * @property TbPelanggan $kdPel
 */
class Favorit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_favorit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_pel', 'kd_barang'], 'required'],
            [['kd_pel', 'kd_barang'], 'integer'],
            [['kd_barang'], 'exist', 'skipOnError' => true, 'targetClass' => Barang::className(), 'targetAttribute' => ['kd_barang' => 'kd_barang']],
            [['kd_pel'], 'exist', 'skipOnError' => true, 'targetClass' => Pelanggan::className(), 'targetAttribute' => ['kd_pel' => 'kode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_pel' => 'Kd Pel',
            'kd_barang' => 'Kd Barang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdBarang()
    {
        return $this->hasOne(TbBarang::className(), ['kd_barang' => 'kd_barang']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdPel()
    {
        return $this->hasOne(TbPelanggan::className(), ['kode' => 'kd_pel']);
    }
}
