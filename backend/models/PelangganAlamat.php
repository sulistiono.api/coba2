<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_pelanggan_alamat".
 *
 * @property int $kode
 * @property int $kode_kirim
 * @property string $nama
 * @property string $jk
 * @property string $alamat
 * @property string $kodepos
 * @property string $provinsi
 * @property string $kota_kabupaten
 * @property string $kecamatan
 * @property string $telp
 * @property int $kode_pel
 * @property string $dropsipe
 * @property string $status
 *
 * @property TbPelanggan $kodePel
 */
class PelangganAlamat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_pelanggan_alamat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_kirim', 'nama', 'jk', 'alamat', 'kodepos', 'provinsi', 'kota_kabupaten', 'kecamatan', 'telp', 'kode_pel', 'dropsipe', 'status'], 'required'],
            [['kode_kirim', 'kode_pel'], 'integer'],
            [['nama', 'dropsipe'], 'string', 'max' => 100],
            [['jk'], 'string', 'max' => 10],
            [['alamat'], 'string', 'max' => 150],
            [['kodepos'], 'string', 'max' => 5],
            [['provinsi', 'kota_kabupaten', 'kecamatan'], 'string', 'max' => 50],
            [['telp'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 20],
            [['kode_pel'], 'exist', 'skipOnError' => true, 'targetClass' => Pelanggan::className(), 'targetAttribute' => ['kode_pel' => 'kode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'kode_kirim' => 'Kode Kirim',
            'nama' => 'Nama',
            'jk' => 'Jk',
            'alamat' => 'Alamat',
            'kodepos' => 'Kodepos',
            'provinsi' => 'Provinsi',
            'kota_kabupaten' => 'Kota Kabupaten',
            'kecamatan' => 'Kecamatan',
            'telp' => 'Telp',
            'kode_pel' => 'Kode Pel',
            'dropsipe' => 'Dropsipe',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodePel()
    {
        return $this->hasOne(Pelanggan::className(), ['kode' => 'kode_pel']);
    }
}
