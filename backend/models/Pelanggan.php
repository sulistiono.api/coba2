<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_pelanggan".
 *
 * @property int $kode
 * @property int $kode_kirim
 * @property string $nama
 * @property string $jk
 * @property string $alamat
 * @property string $kodepos
 * @property string $provinsi
 * @property string $kota_kabupaten
 * @property string $kecamatan
 * @property string $telp
 * @property string $email
 * @property string $password
 * @property string $status
 *
 * @property TbFavorit[] $tbFavorits
 * @property TbKeranjang[] $tbKeranjangs
 * @property TbPelangganAlamat[] $tbPelangganAlamats
 */
class Pelanggan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_pelanggan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_kirim', 'nama', 'jk', 'alamat', 'kodepos', 'provinsi', 'kota_kabupaten', 'kecamatan', 'telp', 'email', 'password', 'status'], 'required'],
            [['kode_kirim'], 'integer'],
            [['nama', 'email', 'password'], 'string', 'max' => 100],
            [['jk'], 'string', 'max' => 10],
            [['alamat'], 'string', 'max' => 150],
            [['kodepos'], 'string', 'max' => 5],
            [['provinsi', 'kota_kabupaten', 'kecamatan'], 'string', 'max' => 50],
            [['telp'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'kode_kirim' => 'Kode Kirim',
            'nama' => 'Nama',
            'jk' => 'Jk',
            'alamat' => 'Alamat',
            'kodepos' => 'Kodepos',
            'provinsi' => 'Provinsi',
            'kota_kabupaten' => 'Kota Kabupaten',
            'kecamatan' => 'Kecamatan',
            'telp' => 'Telp',
            'email' => 'Email',
            'password' => 'Password',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbFavorits()
    {
        return $this->hasMany(TbFavorit::className(), ['kd_pel' => 'kode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbKeranjangs()
    {
        return $this->hasMany(TbKeranjang::className(), ['kd_pel' => 'kode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbPelangganAlamats()
    {
        return $this->hasMany(TbPelangganAlamat::className(), ['kode_pel' => 'kode']);
    }
}
