<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_testimoni".
 *
 * @property int $id
 * @property string $nama
 * @property string $isi
 * @property int $kd_pesan
 * @property string $tanggal
 * @property string $status
 *
 * @property TbPesanan $kdPesan
 */
class Testimoni extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_testimoni';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'isi', 'kd_pesan', 'tanggal', 'status'], 'required'],
            [['kd_pesan'], 'integer'],
            [['tanggal'], 'safe'],
            [['nama'], 'string', 'max' => 50],
            [['isi'], 'string', 'max' => 250],
            [['status'], 'string', 'max' => 10],
            [['kd_pesan'], 'exist', 'skipOnError' => true, 'targetClass' => Pesanan::className(), 'targetAttribute' => ['kd_pesan' => 'kd_pesan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'isi' => 'Isi',
            'kd_pesan' => 'Kd Pesan',
            'tanggal' => 'Tanggal',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdPesan()
    {
        return $this->hasOne(Pesanan::className(), ['kd_pesan' => 'kd_pesan']);
    }
}
