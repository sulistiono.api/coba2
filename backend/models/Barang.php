<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_barang".
 *
 * @property int $kd_barang
 * @property string $nama
 * @property int $jenis
 * @property int $toko
 * @property int $harga
 * @property int $harga_promo
 * @property string $status
 * @property double $berat
 * @property int $stok
 * @property string $gambar
 * @property string $deskripsi
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TbJenis $jenis0
 * @property TbToko $toko0
 * @property TbFavorit[] $tbFavorits
 * @property TbTestimoni[] $tbTestimonis
 */
class Barang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public static function tableName()
    {
        return 'tb_barang';
    }

    /**
     * {@inheritdoc}
     */
    
    public function rules()
    {
        return [
            [['nama', 'jenis', 'toko', 'harga', 'harga_promo', 'status', 'berat', 'stok', 'gambar', 'deskripsi', 'created_at', 'updated_at'], 'required'],
            [['jenis', 'toko', 'harga', 'harga_promo', 'stok', 'created_at', 'updated_at'], 'integer'],
            [['berat'], 'number'],
            
            [['nama'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 10],
            [['gambar', 'deskripsi'], 'string', 'max' => 1000],
            [['jenis'], 'exist', 'skipOnError' => true, 'targetClass' => Jenis::className(), 'targetAttribute' => ['jenis' => 'kd_jenis']],
            [['toko'], 'exist', 'skipOnError' => true, 'targetClass' => Toko::className(), 'targetAttribute' => ['toko' => 'kd_toko']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_barang' => 'Kd Barang',
            'nama' => 'Nama',
            'jenis' => 'Jenis',
            'toko' => 'Toko',
            'harga' => 'Harga',
            'harga_promo' => 'Harga Promo',
            'status' => 'Status',
            'berat' => 'Berat',
            'stok' => 'Stok',
            'gambar' => 'Gambar',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenis0()
    {
        return $this->hasOne(Jenis::className(), ['kd_jenis' => 'jenis']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToko0()
    {
        return $this->hasOne(Toko::className(), ['kd_toko' => 'toko']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbFavorits()
    {
        return $this->hasMany(Favorit::className(), ['kd_barang' => 'kd_barang']);
    }

    
}
