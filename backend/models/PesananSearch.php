<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Pesanan;

/**
 * PesananSearch represents the model behind the search form of `backend\models\Pesanan`.
 */
class PesananSearch extends Pesanan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_pesan', 'kd_pel', 'total_potongan', 'kode_unik', 'total_bayar', 'total_ongkir', 'kd_tranfer', 'jumlah_tranfer'], 'integer'],
            [['alamat', 'detail_po', 'no_resi', 'no_rek_pel', 'no_rek_sonya', 'tgl_transfer', 'status'], 'safe'],
            [['total_berat'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pesanan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kd_pesan' => $this->kd_pesan,
            'kd_pel' => $this->kd_pel,
            'total_potongan' => $this->total_potongan,
            'kode_unik' => $this->kode_unik,
            'total_berat' => $this->total_berat,
            'total_bayar' => $this->total_bayar,
            'total_ongkir' => $this->total_ongkir,
            'kd_tranfer' => $this->kd_tranfer,
            'jumlah_tranfer' => $this->jumlah_tranfer,
            'tgl_transfer' => $this->tgl_transfer,
        ]);

        $query->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'detail_po', $this->detail_po])
            ->andFilterWhere(['like', 'no_resi', $this->no_resi])
            ->andFilterWhere(['like', 'no_rek_pel', $this->no_rek_pel])
            ->andFilterWhere(['like', 'no_rek_sonya', $this->no_rek_sonya])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
