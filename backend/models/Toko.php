<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_toko".
 *
 * @property int $kd_toko
 * @property string $nama_toko
 *
 * @property TbBarang[] $tbBarangs
 */
class Toko extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_toko';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_toko'], 'required'],
            [['nama_toko'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_toko' => 'Kd Toko',
            'nama_toko' => 'Nama Toko',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbBarangs()
    {
        return $this->hasMany(TbBarang::className(), ['toko' => 'kd_toko']);
    }
}
